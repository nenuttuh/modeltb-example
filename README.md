# modeltb-example

This is an example project to demonstrate the usage of [modeltb](https://bitbucket.org/nenuttuh/modeltb/) library.

## Installing modeltb

Modeltb is istalled with pip. At the moment modeltb is not available in PyPI, so it needs to be installed from the git repository.
In this example we install dependencies from `requirements.txt`. We will use sklearn data and functions, so it will be required too.

First, you should create a virtual environment to avoid installing the packages globally, e.g. using Python standard library's [venv](https://docs.python.org/3/library/venv.html) module.

To install dependencies, run in the project root:

`pip install -r requirements.txt`

and your good to go.

## Usage

For a model to be testable, there are three essential abstract classes that need to be instantiated: Testbench, Model and Dataset. If a new testbench with associated model and dataset is found in the database, TBRunner will run the testbench automatically (if the runner is started).

Before you can use modeltb you need to create the database. The database (and other files and folders modeltb generates) will be created in `MODELTB_HOME` folder, which by default will be `~/modeltb`. To choose another location, set `MODELTB_HOME` environment variable:

`export MODELTB_HOME=/path/to/wanted/location`

There is a utility script to set `MODELTB_HOME` to `/projectroot/modeltb`. In the project root, run

`source set_local_env.sh`

The modelb CLI needs to be provided with a module that includes all of the classes that inherit from modeltb base classes. It will import this module so that the custom classes are defined in modeltb.models.Base when running database operations. In this example, the module is `modeltb_example`, which imports `models.py` in its `__init__.py`. You need to run modeltb commands from project root dir.

The database can be created with:

`modeltb createdb modeltb_example`

To start the testbench runner with three workers (you can choose other number to increase or decrease parallel testbench execution):

`modeltb run --num_workers=3 modeltb_example`

Then, whenever a Testbench instance is created into database, the runner whould pick it up. To create a testbench with classes from `modeltb_example/models.py`:

```python
import modeltb
from modeltb_example import models

dataset = models.IrisDataset()
model = models.IrisModel()
tb = IrisTb(model=model, dataset=dataset)

session = modeltb.settings.Session()
session.add_all([dataset, model, tb])
session.commit()
```

A full example can be found in `create_and_run_tb.py`. You can run the script with `python create_and_run_tb.py`.

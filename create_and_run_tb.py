import modeltb
from modeltb_example import models

# Create the objects
dataset = models.IrisDataset()
model = models.IrisModel()
tb = models.IrisTb(model=model, dataset=dataset)

# Add objects to session, and commit to insert into db
session = modeltb.settings.Session()
session.add_all([dataset, model, tb])
session.commit()

# Run the testbench
tb.run()

# Check that the testbench was succeeded
session.add(tb)
assert tb.state == 'SUCCESS'
print("%s with id %s succeeded with score %s" % (tb.__class__.__name__, tb.id, tb.score))

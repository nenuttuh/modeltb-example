# Import modules that inherit from modeltb base classes.
# This way modeltb can find them, when you do
# 'modeltb createdb modeltb_example'
# Otherwise you would need to define the full path, i.e.
# 'modeltb createdb modeltb_example.models'
from modeltb_example import models

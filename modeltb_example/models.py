"""Example how to extend modeltb base classes.
The iris data problem taken from
http://scikit-learn.org/stable/tutorial/basic/tutorial.html
"""
from modeltb import models
from sqlalchemy import Column, Integer, ForeignKey
from sklearn import datasets, svm
from sklearn.model_selection import train_test_split as sk_split
import math
import time

# If you want to log to modeltb logs, create the logger like this
import logging
logger = logging.getLogger(__name__)


class IrisTb(models.Testbench):
    """This is the user defined testbench class.
    
    You need to inherit the class from modeltb.models.Testbench 
    (or from a hierarchy that inherits from Testbench).
    """
    # Id needs to be defined as foreign key to Testbench.id so that
    # polymorphism works correctly. Note that 'testbench' refers to
    # the database table so it's lowercase.
    id = Column(Integer, ForeignKey('testbench.id'), primary_key=True)

    # __mapper_args__ is a dict that SQLAlchemy will use to resolve
    # polymorpism related issues. Just set polymorphic_identity to
    # the table name.
    __mapper_args__ = {'polymorphic_identity': 'iristb'}

    @classmethod
    def get_score(self, y_pred, test_data):
        """You need to define the get_score method.
        
        The method will calculate the score from predicted and true labels.
        """
        y_true = test_data[1]
        assert len(y_pred) == len(y_true)
        return sum(y_pred == y_true) / len(y_pred)

    def save_results(self, y_pred, test_data):
        """You also need to define save_results method.
        
        With it you can save additional details, e.g. other metrics or
        model weights. Let's just leave it empty so that modeltb saves
        whatever it saves...
        """
        pass


class IrisModel(models.Model):
    """The model extended from modeltb.models.Testbench.
    
    At minimum, implement fit() and predict() methods.
    """
    # id and __mapper_args__ go the same as in above
    id = Column(Integer, ForeignKey('model.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'irismodel'}

    clf = None

    def fit(self, train_data):
        """Fit a linear SVM to the data."""
        xs, ys = train_data
        self.clf = svm.SVC()
        self.clf.fit(xs, ys)
        # Sleep for a moment to demonstrate how runner handles running tbs
        time.sleep(15)

    def predict(self, test_data):
        """Use the fitted model and return predictions."""
        xs = test_data[0]
        if self.clf is None:
            raise Exception("self.clf is not defined!")
        return self.clf.predict(xs)


class IrisDataset(models.Dataset):
    """This is the dataset interface that needs to be implemented.
    
    At minimum, implement train_test_split() method.
    """
    id = Column(Integer, ForeignKey('dataset.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'irisdataset'}

    def train_test_split(self, test_size=0.1, seed=None):
        """Get the data and return them splitted according to test_size.
        
        test_size=0.1 means that 10% of the data will be used as test data.

        Returns:
            (X_train, X_test, y_train, y_test) i.e. datasets as a tuple
        """
        dataset = datasets.load_iris()
        Xs, ys = dataset.data, dataset.target
        x_train, x_test, y_train, y_test = sk_split(Xs, ys, test_size=test_size, random_state=seed)

        return (x_train, y_train), (x_test, y_test)
